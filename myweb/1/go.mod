module myweb

go 1.15

require (
	github.com/astaxie/beego v1.12.3
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/protobuf v1.5.2
	github.com/gomodule/redigo v2.0.0+incompatible
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)
