package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
	"myweb/controllers/MRI"
	"myweb/middleware"
)

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowAllOrigins:  true,
		AllowOrigins:     []string{"http://36.133.194.213"},
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type", "X-TOKEN"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		AllowCredentials: true,
	}))
	beego.Router("/login",&MRI.LoginController{},"*:HandleLogin")
	beego.Router("/sendEmail",&MRI.SendEmail{},"*:SendEmailToUser")
	beego.Router("/register",&MRI.Register{},"*:Register")
	ns:=beego.NewNamespace("/"+beego.AppConfig.String("front"),   //csg
			beego.NSBefore(middleware.FilterToken),
			beego.NSRouter("/pfistaMul",&MRI.PFISTAMul{},"post:HanderMul"),
			beego.NSRouter("/pfistaMul_pngky",&MRI.PFISTAPngKy{},"get:HanderPngKy"),
			beego.NSRouter("/pfistaMul_pngkx",&MRI.PFISTAPngKx{},"get:HanderPngKx"),
			beego.NSRouter("/pfistaMul_result",&MRI.PFISTAMulRes{},"get:SearchRes"),
			beego.NSRouter("/pfistaMul_delete",&MRI.PFISTADel{},"get:HanderDel"),
			beego.NSRouter("/pfistaMul_png",&MRI.PFISTAPng{},"get:HanderPng"),
			beego.NSRouter("/pfistaMul_evl",&MRI.PFISTAMulEvl{},"get:GetEvl"),
			beego.NSRouter("/file/upload", &MRI.FileUploadController{}, "get:Upload;post:Upload"),
			beego.NSRouter("/file/merge", &MRI.FileUploadController{}, "get:Merge;post:Merge"),
	)
	beego.AddNamespace(ns)

}
