package MRI

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"myweb/models"
	"strconv"
	"strings"
)

type PFISTAMulEvl struct {
	beego.Controller
}
func StrsToStr(index string)string{

	var temp string
	temp = strings.Replace(index, "[", "", -1)
	temp = strings.Replace(temp, "]", "", -1)
	return temp
}
func (c *PFISTAMulEvl) GetEvl(){
	//tokenData:=c.Ctx.Input.Header("Authorization")
	//result,_:=middleware.HandleToken(tokenData)
	//if result==false{ //没有token,无法提供服务
	//	c.Data["json"]= map[string]interface{}{
	//		"status_code":404,
	//	}
	//	c.ServeJSON()
	//}
	fmt.Print("start evl png")
	id,err1:=c.GetInt("id")
	dia,_:=c.GetInt("dia")
	fmt.Println("dia is :",dia)
	if err1!=nil{
		fmt.Println("获取id错误,err:",err1)
		c.Data["json"]= map[string]interface{}{
			"status_code":"failure",
		}
		c.ServeJSON()
	}
	fmt.Println("id",id)
	tempId:=strconv.Itoa(id)+strconv.Itoa(dia)
	idInt,_:=strconv.Atoi(tempId)
	index1:= c.GetStrings("index1")
	fmt.Println("index1",index1[0])
	index2:= c.GetStrings("index2")
	index3:= c.GetStrings("index3")
	index4:= c.GetStrings("index4")
	index5:= c.GetStrings("index5")
	index6:= c.GetStrings("index6")
	o:=orm.NewOrm()
	data:=models.Evl{Id:int64(idInt)}
	err:=o.Read(&data)
	if err!=nil{
		fmt.Println("查询数据库失败")
		c.Data["json"]= map[string]interface{}{
			"status_code":"failure",
		}
		c.ServeJSON()
	}else {
		data.Index1=StrsToStr(index1[0])
		fmt.Println("StrsToStr(index1[0])",StrsToStr(index1[0]))
		data.Index2=StrsToStr(index2[0])
		data.Index3=StrsToStr(index3[0])
		data.Index4=StrsToStr(index4[0])
		data.Index5=StrsToStr(index5[0])
		data.Index6=StrsToStr(index6[0])
	}
	_,err=o.Update(&data)
	if err!=nil{
		fmt.Println("数据库更新失败,err:",err)
		c.Data["json"]= map[string]interface{}{
			"status_code":"failure",
		}
		c.ServeJSON()
	}
	c.Data["json"]= map[string]interface{}{
		"status_code":"success",
	}
	c.ServeJSON()
}
