package MRI

import (
	"context"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"myweb/controllers/pb"
	"myweb/controllers/utils"

	"myweb/middleware"
	"myweb/models"
	"strconv"
	"time"
)

type PFISTAMul struct{
	beego.Controller
}

func GenerateZero(page int)string{

	var temp string
	zero:="0"
	for i:=0;i<int(page);i++{
		if i==0{ //开始不用逗号
			temp=temp+zero
			continue
		}
		temp=temp+","+zero
	}
	fmt.Println("tempZero",temp)
	return temp
}

func (c *PFISTAMul) HanderMul (){

	tokenData:=c.Ctx.Input.Header("Authorization")
	result,_:=middleware.HandleToken(tokenData)
	if result==false{ //没有token,无法提供服务
		c.Data["json"]= map[string]interface{}{
			"status_code":404,
		}
		c.ServeJSON()
	}
	log:=utils.CreateLog("PFISTA_MulHandel.log")
	fmt.Println("开始接收数据")

	filename:=c.GetString("filename")
	userName:=c.GetString("username")
	lamba,_:=c.GetFloat("lamba")
	lambaStr:= strconv.FormatFloat(lamba, 'E', -1, 64)
	rawDataUrl:=c.GetString("rawDataUrl")
	maskUrl:=c.GetString("maskUrl")
	//fmt.Println(filename)
	//fmt.Println("lamba",lamba)
	//f1, h1, err1 := c.GetFile("data")
	//
	//if err1 != nil {
	//	log.Error("file1上传失败失败:", err1)
	//	return
	//}
	//defer f1.Close()
	//f2, h2, err2 := c.GetFile("mask")
	//if err2 != nil {
	//	log.Error("file2上传失败失败:", err1)
	//	return
	//}
	//defer f2.Close()
	//fileExt1 := path.Ext(h1.Filename)
	//fileExt2 := path.Ext(h2.Filename)
	//log.Informational(fileExt1)
	//log.Informational(fileExt2)
	//filename1 := strconv.FormatInt(time.Now().Unix(), 10)
	//filenameSave1 := filename1 + fileExt1 //精准到秒，文件名就不会重复
	//filenameSave2 := filename1 + fileExt2 //精准到秒，文件名就不会重复
	//
	//_ = c.SaveToFile("data", "./static/pFISTAMul/data/"+filenameSave1) //存储成功
	//_ = c.SaveToFile("mask", "./static/pFISTAMul/mask/"+filenameSave2) //存储成功
	//fmt.Println("文件保存成功")
	//transportAdd1:="/root/go/myweb/static/pFISTAMul/data/"+filenameSave1
	//transportAdd2:="/root/go/myweb/static/pFISTAMul/mask/"+filenameSave2
	transportAdd1:=rawDataUrl
	fmt.Println("radDataUrl",rawDataUrl)
	transportAdd2:=maskUrl
	fmt.Println("maskUrl",maskUrl)
	/***************************************************************************/
	/*grpc传输*/
	////1.连接
	//conn, err := grpc.Dial("127.0.0.1:10086", grpc.WithInsecure())
	//if err != nil {
	//	log.Error("连接服务器失败")
	//	return
	//}
	//defer conn.Close()
	////2.实例化gRPC客户端
	//client := pb.NewGreeterClient(conn)   //使用连接池的client

	//md := metadata.Pairs("data_path", transportAdd1, "mask_path",transportAdd2,"lamba",lambaStr) //传递给另一端key-value,filename1不带.mat标识，只为了给py写路径使用
	//ctx := metadata.NewOutgoingContext(context.Background(), md)
	//fmt.Println("transport:-----------------")
	//fmt.Println(transportAdd1)
	//fmt.Println(transportAdd2)
	//ans, err := client.Evaluate(ctx, &pb.EvlRequest{DataPath: transportAdd1, MaksPath: transportAdd2,Lamba: lambaStr}) //如果不需要响应，则不需要ans
	//if err != nil {
	//	log.Error("can not get the info,err:%s\n", err)
	//	return
	//}
	/*----------------------------------------------------------------------*/
	grpcConnect:=GetGrpcConnection()
	client:=pb.NewGreeterClient(grpcConnect.conn)
	defer  func() {
		//释放资源，即往通道+数量
		grpcConnect.signal <-0
		if err:=recover();err!=nil{
			log.Informational("can not recover,panic err:",err)
		}
	}()
	ctx, cancel := context.WithTimeout(context.Background(), 8*time.Minute)
	defer cancel()
	ans,err:=client.Evaluate(ctx,&pb.EvlRequest{
		DataPath: transportAdd1,
		MaksPath: transportAdd2,
		Lamba: lambaStr,
	})
	time_code := ans.TimeCode
	page_num :=ans.PageNumber
	row :=ans.Row
	col :=ans.Col

	/*----------------------------------------------------------------------*/

	o := orm.NewOrm()
	saveToSql := models.PfistaMulti{}
	scoreTable := models.Score{}
	scoreKy := models.ScoreKy{}
	scoreKx := models.ScoreKx{}
	evlTable1 := models.Evl{}
	evlTable2 := models.Evl{}
	evlTable3 := models.Evl{}
	sizeTable := models.Size{}
	idString := strconv.FormatInt(time.Now().Unix(), 10)
	idInt, err := strconv.Atoi(idString)
	if err != nil {
		log.Error("can not exchange string to int,err:", err)
	}
	saveToSql.Id = idInt
	saveToSql.Username=userName
	saveToSql.Filename=filename+".mat"
	saveToSql.ResAddress="/resmat/"+time_code+".mat"
	saveToSql.ResUWTPng="/resReconUWT/"+time_code+"/"+time_code+"_"
	saveToSql.ResDWTPng="/resReconDWT/"+time_code+"/"+time_code+"_"
	saveToSql.RealAddress="/resReal/"+time_code+"/"+time_code+"_"
	saveToSql.Data=rawDataUrl
	//saveToSql.Mask="/static/pFISTAMul/data/"+filenameSave2
	saveToSql.Mask=maskUrl
	saveToSql.Score=0   //生成图片的同时打0分
	saveToSql.Page=int(page_num)
	//返回插入的几条数据，err
	/*******************FindPng页面(点击评分按钮请求的数据)*******************/
	scoreTable.Id=idInt
	scoreTable.RealAddress="/resReal/"+time_code+"/"+time_code+"_"
	scoreTable.ReconUWTAddress="/resReconUWT/"+time_code+"/"+time_code+"_"
	scoreTable.ReconDWTAddress="/resReconDWT/"+time_code+"/"+time_code+"_"
	scoreTable.Page=int(page_num)
	/********************************************************************/
	/*******************FindPng页面(点击评分按钮请求的数据)*******************/
	scoreKy.Id=idInt
	scoreKy.RealAddressKy="/resRealKy/"+time_code+"/"+time_code+"_"
	scoreKy.ReconUWTAddressKy="/resReconUWTKy/"+time_code+"/"+time_code+"_"
	scoreKy.ReconDWTAddressKy="/resReconDWTKy/"+time_code+"/"+time_code+"_"
	scoreKy.Page=int(row)
	/********************************************************************/
	/*******************FindPng页面(点击评分按钮请求的数据)*******************/
	scoreKx.Id=idInt
	scoreKx.RealAddressKx="/resRealKx/"+time_code+"/"+time_code+"_"
	scoreKx.ReconUWTAddressKx="/resReconUWTKx/"+time_code+"/"+time_code+"_"
	scoreKx.ReconDWTAddressKx="/resReconDWTKx/"+time_code+"/"+time_code+"_"
	scoreKx.Page=int(col)
	/********************************************************************/
	kz,_:=strconv.Atoi(idString+"1")
	kzZero:=GenerateZero(int(page_num))
	fmt.Println("kzId",kz)
	evlTable1.Id=int64(kz)
	evlTable1.Index1=kzZero
	evlTable1.Index2=kzZero
	evlTable1.Index3=kzZero
	evlTable1.Index4=kzZero
	evlTable1.Index5=kzZero
	evlTable1.Index6=kzZero
	/********************************************************************/
	_,err6:=o.Insert(&evlTable1)
	if err6!=nil{
		fmt.Println("不能插入数据库，err:",err6)
	}
	/********************************************************************/
	ky,_:=strconv.Atoi(idString+"2")
	fmt.Println("kyId",ky)
	kyZero:=GenerateZero(int(row))
	evlTable2.Id=int64(ky)
	evlTable2.Index1=kyZero
	evlTable2.Index2=kyZero
	evlTable2.Index3=kyZero
	evlTable2.Index4=kyZero
	evlTable2.Index5=kyZero
	evlTable2.Index6=kyZero
	/********************************************************************/
	_,err =o.Insert(&evlTable2)
	/********************************************************************/
	kx,_:=strconv.Atoi(idString+"3")
	fmt.Println("kxId",kx)
	kxZero:=GenerateZero(int(col))
	evlTable3.Id=int64(kx)
	evlTable3.Index1=kxZero
	evlTable3.Index2=kxZero
	evlTable3.Index3=kxZero
	evlTable3.Index4=kxZero
	evlTable3.Index5=kxZero
	evlTable3.Index6=kxZero
	/********************************************************************/
	sizeTable.Id=idInt
	sizeTable.Row=int(row)
	sizeTable.Col=int(col)
	sizeTable.Slice=int(page_num)
	/********************************************************************/
	_, err = o.Insert(&saveToSql)
	_,err =o.Insert(&scoreTable)
	_,err7:=o.Insert(&evlTable3)
	if err7!=nil{
		fmt.Println("无法插入数据库,err:",err7)
	}
	_,err =o.Insert(&sizeTable)
	_,err =o.Insert(&scoreKx)
	_,err =o.Insert(&scoreKy)
	if err != nil {
		log.Error("插入数据错误:", err)
		return
	}

	//4.插入成功返回文章界面
	log.Flush() // 将日志从缓冲区读出，写入到文件
	defer log.Close()
	c.Data["json"]= map[string]interface{}{
		"status_code":200,
	}
	c.ServeJSON()

}






