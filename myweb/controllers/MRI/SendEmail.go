package MRI

import (
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/jordan-wright/email"
	"math/rand"
	"net/smtp"
	"time"
)
type SendEmail struct {
	beego.Controller

}
var (
	pool *email.Pool
	maxClient int = 10
)
func SendEmailWithPool(from string,to []string, secret string, host string, nickname string,subject string, body string, port int) (err error) {
	hostAddr := fmt.Sprintf("%s:%d", host, port)
	auth := smtp.PlainAuth("", from, secret, host)
	if pool == nil {
		pool, err = email.NewPool(hostAddr, maxClient, auth)
		if err != nil {
			fmt.Println(err)
		}
	}
	e := &email.Email{
		From: fmt.Sprintf("%s<%s>", nickname, from),
		To:      to,
		Subject: subject,
		HTML:    []byte(body),
	}
	return pool.Send(e, 5 * time.Second)
}
func sendEmailTest1(toPe string) (ver_code string ,err error) {
	//getName:="dcchen@stu.xmu.edu.cn"
	//var to  = []string{"dcchen@stu.xmu.edu.cn","blakezyr@163.com","guoyi3299@live.com"}
	var to  = []string{toPe}
	from := "1102697667@qq.com"
	nickname := "CSG Medical Platform Registration Verification Code"
	secret := "scdfjsypblcyhiei"
	host := "smtp.qq.com"
	port := 25
	subject := "Verification Code"
	rand.Seed(time.Now().UTC().UnixNano())
	rnd:=rand.New(rand.NewSource(time.Now().UnixNano()))
	vcode:=fmt.Sprintf("%06v",rnd.Int31n(1000000))
	fmt.Println(vcode)
	body := "<h1 style=\"color: red\">"+vcode+"<h1>"
	if err := SendEmailWithPool(from, to, secret, host, nickname, subject, body, port); err != nil{
		fmt.Println("发送失败: ", err)
		return "",err
	}else {
		fmt.Println("发送成功")
		return vcode,nil
	}
}
type Email struct {
	Email string `json:"email"`
}
func (c *SendEmail) SendEmailToUser(){
	email:=&Email{}
	json.Unmarshal(c.Ctx.Input.RequestBody,&email)

	fmt.Println(email.Email)
	vcode,err:=sendEmailTest1(email.Email)
	if err!=nil{
		c.Data["json"]= map[string]interface{}{
			"status_code":"failure",
		}
		c.ServeJSON()
	}
	c.SetSession("vcode",vcode)
	c.Data["json"]= map[string]interface{}{
		"status_code":"success",
	}
	c.ServeJSON()
}