package MRI

import (
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"myweb/models"
)

type PFISTAMulRes struct{
	beego.Controller
}
type LogT struct {
	Filename 	string 	`json:"filename"`
	Level 		int64 	`json:"level"`
	Maxlines 	int64 	`json:"maxlines"`
	Maxsize 	int64 	`json:"maxsize"`
}
func (c *PFISTAMulRes) SearchRes() {
	/*****************************设置本地日志**************************************/
	log := logs.NewLogger(100)
	cfgLog := &LogT{}
	cfgLog.Filename = "result.log"
	cfgLog.Level = logs.LevelDebug
	cfgLog.Maxlines = 1000
	cfgLog.Maxsize = 10240
	cfgJson, _ := json.Marshal(cfgLog)
	logs.Info(cfgJson)
	log.SetLogger("file", string(cfgJson)) //设置记录日志的方式，本地文件记录
	log.EnableFuncCallDepth(true)          // 输出log时能显示输出文件名和行号（非必须）
	/****************************************************************************/
	//tokenData:=c.Ctx.Input.Header("Authorization")
	//result1,err:=middleware.HandleToken(tokenData)
	//if result1==false{ //没有token,无法提供服务
	//	log.Error("无法显示result,err:",err)
	//	c.Data["json"]= map[string]interface{}{
	//		"status_code":404,
	//	}
	//	c.ServeJSON()
	//}
	o:=orm.NewOrm()
	//存储查询结果
	var result []models.PfistaMulti
	var final []models.PfistaMulti
	qs:=o.QueryTable("PfistaMulti").OrderBy("-id")
	count,err:=qs.Count()
	if err!=nil{
		logs.Info("查询错误")
		return
	}
	logs.Informational("表中数据一共有:",count)

	//获取总页数

	pageIndex,_:=c.GetInt("pageNum")
	pageSize,_:=c.GetInt("pageSize")
	userName :=c.GetString("username")
	
	start:=pageSize*(pageIndex-1)
	qs.Limit(pageSize,start).All(&result)    //1.pagesize 一页显示多少 2.start 从哪开始查

	fmt.Println("len(result)",len(result))

	for i:=0;i<len(result);i++{
		if result[i].Username==userName{
			final=append(final,result[i])
		}
	}
	fmt.Println("len(final)",len(final))

	c.Data["json"]= map[string]interface{}{
		"result":final,
		"count":count,
	}
	c.ServeJSON()
}