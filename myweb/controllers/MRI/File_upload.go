package MRI

import (
	"bufio"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/astaxie/beego"
	"io"
	"io/ioutil"
	"myweb/models"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"time"
)

type FileUploadController struct {
	beego.Controller

}

type FileUploadInfo struct {
	Id               int    `json:"id"`
	ChunkNumber      int    `json:"chunkNumber"`      // 当前是第几片
	ChunkSize        int    `json:"chunkSize"`        // 每片的大小
	CurrentChunkSize int    `json:"currentChunkSize"` // 当前分片的大小
	TotalSize        int    `json:"totalSize"`        // 文件的总大小
	TotalChunks      int    `json:"totalChunks"`      // 总分片数
	FileName         string `json:"fileName"`
	Identifier       string `json:"identifier"` 	  // fileMd5值
	ChunkNumbers	 int	`json:"chunkNumbers"`
	//HasBeenUploaded  string `json:"hasBeenUploaded"`
}

// 响应信息
type FileUploadResponseInfo struct {
	IsUploaded      int    `json:"isUploaded"`      // 是否已经完成上传了 0:否  1:是 - 秒传
	HasBeenUploaded string `json:"hasBeenUploaded"` // 曾经上传过的分片chunkNumber - 断点续传
	Merge           int    `json:"merge"`           // 是否可以合并了   0：否  1:是
	Status          int    `json:"status"`          // 是否可以合并了   0：成功  1:失败
	Msg             string `json:"msg"`             // 其他信息
	URL             string `json:"url"`             // 后端保存的url
}
var(
	mutex sync.Mutex
)
/**
 *  约定：分片的命名规则    /upload/username/fileMd5_chunkNumber.Ext
 *  约定：merge后的文件名  /upload/username/fileMd5.Ext
 */
func (c *FileUploadController) Upload()  {
	//返回前端信息的结构体
	resultInfo :=FileUploadResponseInfo{}
	//接收，处理数据
	filename:=c.GetString("filename")
	chunkNumber,_:=c.GetInt("chunkNumber")
	currentChunkSize,_:=c.GetInt("currentChunkSize")
	totalChunks,_:=c.GetInt("totalChunks")
	fileMd5:=c.GetString("identifier")
	username:=c.GetString("username")
	tempFileName:=fileMd5+"_"+strconv.Itoa(chunkNumber)+filepath.Ext(filename)		//每个分片的名字
	targetFileName:=fileMd5+filepath.Ext(filename)									//合并后的文件名
	//分片校验
	method:=c.Ctx.Request.Method

	//上传之前预检验
	if method=="GET"{ //Get请求是为了查询后端已经上传过哪些chunk，进行断点续传
		detail:=models.FileUploadDetail{}
		//查询时也需要原子操作
		detail,err:=detail.FindUploadDetailByFileName(username,targetFileName)
		//查询不到记录，说明这是第一次上传
		if err!=nil && err.Error() == "<QuerySeter> no row found"{//第一个chunk的判断，需要写入数据库
			//更新数据库的hasBeenUploaded
			detail:=models.FileUploadDetail{
				Username:			username,
				FileName:			targetFileName,
				Md5:				fileMd5,
				TotalChunks:		totalChunks,
				IsUploaded:			0,   //0表示还未上传
				//HasBeenUploaded: 	"",
				//ChunkNumbers:  		0,    //用于统计已经上传过的chunk数量
				CreateTime:			time.Now(),
				UpdateTime:			time.Now(),
			}
			id,err:=detail.InsertOneRecord()
			if err!=nil || id<0{
				fmt.Printf("fail to insert upload detail record fineName:[%v] md5:[%v] err:[%v]", filename, fileMd5, err)
			}
			//如果第一个chunk就可以完成任务，那么直接返回
			if chunkNumber==totalChunks{
				resultInfo.IsUploaded=0  //还没有上传完成（合并才算完成）
				resultInfo.Merge=1		 //通知前端可以合并了
				c.Data["json"]=resultInfo
				c.ServeJSON()
				return
			}
			//如果第一个chunk不能完成任务，那么返回不能合并
			resultInfo.IsUploaded=0
			resultInfo.Merge=0
			resultInfo.HasBeenUploaded=""   //为空，说明第一个chunk上传完毕
			c.Data["json"]=resultInfo
			c.ServeJSON()
			return
		}
		//else说明之前已经上传过记录，不是第一个chunk
		//检查当前文件是否已经上传过，并且是否是完整上传（detail是通过数据库查询得到的记录）
		if detail.IsUploaded==1{  //说明已经上传过
			resultInfo.IsUploaded=1
			resultInfo.Merge=0
			resultInfo.URL=detail.Url
			c.Data["json"]=resultInfo
			c.ServeJSON()
			return
		}
		//处理断点续传的chunk
		resultInfo.IsUploaded=0
		resultInfo.Merge=0
		//resultInfo.HasBeenUploaded=detail.HasBeenUploaded	//将已经上传的chunk记录发送给前端
		c.Data["json"]=resultInfo
		c.ServeJSON()
		return
	}


	//post，开始处理上传请求


	detail,err:=models.NewFileUploadDetail().FindUploadDetailByFileName(username,targetFileName)

	if err != nil && err.Error() == "<QuerySeter> no row found" {
		fmt.Printf("post: fail to findUploadDetailByFileName username:[%v] targetFileName:[%v]", username, targetFileName)
		return
	}
	//判断是否已经上传完成，已经完成就不需要其他操作了
	if detail.IsUploaded==1{
		// todo 判断是否曾经merge过
		if len(detail.Url)!=0{ //若存在url，说明之前合并成功过，则不需要再合并一次
			resultInfo.Msg="has been success upload,please require URL:PFISTA_MUL"
			resultInfo.URL=detail.Url
			c.Data["json"]=resultInfo
			c.ServeJSON()
			return
		}
		fmt.Printf("post: username: [%v] has been uploaded the file filename:[%v] md5[%v]",username,targetFileName,fileMd5)
		resultInfo.IsUploaded=1
		resultInfo.Merge=0  //已经完成不需要合并操作
		c.Data["json"]=resultInfo
		c.ServeJSON()
		return
	}
	// todo 当前currentSize为空， 需要特殊处理一下
	//if currentChunkSize<=0{
	//	resultInfo.Msg="currentChunkSize is null,please reupload"
	//	resultInfo.IsUploaded=0
	//	resultInfo.Merge=0
	//	c.Data["json"]=resultInfo
	//	c.ServeJSON()
	//	return
	//}
	//保存当前的chunk

	err,SumChunk:=SaveChunkToLocalFromMutipartForm(c,tempFileName,username,currentChunkSize,fileMd5)
	//c.mutex.Lock()
	//detail.ChunkNumbers+=1
	//fmt.Printf("1++=2、已上传 %v 个切片",detail.ChunkNumbers)
	//SumChunk:=detail.ChunkNumbers
	//detail.UpdateColumn("chunk_numbers")
	//c.mutex.Unlock()
	if err!=nil{
		fmt.Printf("post fail to save chunk fileName:[%v] md5:[%v] chunkNumber:[%v]",filename,fileMd5,chunkNumber)
		//保存失败，通知前端重新发送

		c.Ctx.ResponseWriter.WriteHeader(500)
		resultInfo.IsUploaded = 0
		resultInfo.Merge = 0
		c.Data["json"] = resultInfo
		c.ServeJSON()
		return
	}

	//数据库添加字段 (格式：“1:2:3”)
	//if detail.TotalChunks==chunkNumber{
	//	detail.HasBeenUploaded = detail.HasBeenUploaded + strconv.Itoa(chunkNumber)
	//}else{
	//	detail.HasBeenUploaded =detail.HasBeenUploaded+strconv.Itoa(chunkNumber)+":"
	//}
	//num,err:=detail.UpdateColumn("has_been_uploaded")
	//if err!=nil || num<0{
	//	fmt.Printf("fail to updateColumn err:[%v]",err)
	//	return
	//}
	// 如果前端是并发上传的化，这个顺序不能保证～，所以下面的条件应该是比对 totalChunks == chunkNumber不能保证全部正确
	// 并发的话需要加个if判断一下，当上传过的分片数 == totalChunks 就可以merge了
	// 目前前端会采用单条线程顺序发送chunk，故下面的条件衡成立
	// 服务端判断，当前chunkNumber == totalChunks时, 先保存当前chunk 再向前端发送特殊响应，前端接收到后会发送merge请求

	if SumChunk==totalChunks{
		// 更新数据库 标记文件全部上传完成
		detail.IsUploaded = 1
		num, err := detail.UpdateColumn("is_uploaded")  //更换for update进行行锁，可以实现并发
		if err != nil || num < 0 {
			fmt.Printf("fail to updateColumn err:[%v]", err)
			return
		}
		resultInfo.IsUploaded = 0
		// 告诉前端可以merge了
		resultInfo.Merge = 1
		c.Data["json"] = resultInfo
		c.ServeJSON()
		return
	}
	//还没完成（IsUploaded=0），进行接收，并告诉前端这次切片成功保存了
	resultInfo.IsUploaded = 0
	resultInfo.Merge = 0
	resultInfo.Msg = "ok"
	c.Data["json"] = resultInfo
	c.ServeJSON()
	return
}

/**
 *   desc：
 *       将chunk中的数据暂时存在本地
 *
 *	 params：
 *	 	 tempFileName: 当前分片使用的文件名
 *		 currentChunkSize: 当前分片的大小
 */
func SaveChunkToLocalFromMutipartForm(c *FileUploadController,tempFileName,username string,currentChunkSize int,md5Str string)(err error,fileNum int){
	//创建文件夹
	path:="/opt/data"
	folderPath := path + "/upload/" + username +"/" + md5Str
	if _,err:=os.Stat(folderPath);os.IsNotExist(err){
		fmt.Println("正在创建文件夹")
		//必须分成两步
		//1、先创建文件夹
		os.MkdirAll(folderPath,0777)
		//2、修改权限
		os.Chmod(folderPath,0777)
	}

	// 保存本次上传的分片 /opt/data/upload/username/fileMd5_chunkNumber.Ext
	//第一次请求是查询已经上传过几个切片，所以第一次请求是空的
	if	c.Ctx.Request.MultipartForm==nil{
		return
	}
	fileHeader:=c.Ctx.Request.MultipartForm.File["file"][0]
	if fileHeader==nil{
		err = errors.New("fileHeader 为空")
	}
	file,err:=fileHeader.Open()
	if err!=nil{
		fmt.Printf("error :%v",err)
		return
	}
	//在本地创建文件，如果没有就创建，如果有就打开
	myFile,err:=os.Create(folderPath+"/"+tempFileName)
	if err!=nil{
		fmt.Printf("error:%v",err)
		return
	}

	//循环读取前端发送过来的文件
	buf:=make([]byte,currentChunkSize)//按照chunk的size读取文件
	num,err:=file.Read(buf)
	if err!=nil{
		fmt.Printf("error:%v",err)
		return
	}
	fmt.Printf("本次读取了 num=[%v] byte 的数据",num)

	//保存文件
	num,err=myFile.Write(buf)
	if err!=nil{
		fmt.Printf("error:%v",err)
		return
	}
	fmt.Printf("本次保存的分片size为:%v",num)
	myFile.Close()
	file.Close()
	mutex.Lock()
	files,_ := ioutil.ReadDir(folderPath)
	fmt.Println("文件数量:",len(files))
	mutex.Unlock()
	return nil,len(files)
}
/**
 * desc：merge files
 *
 * params：
 *		fileName
 *	    fileMd5
 *
 * return：
 *		msg: mergeDone:成功合并  /mergeErr：合并过程中出错 /fileMd5Err：fileMd5比对失败了
 *      url: sourceUrl
 */
func (c *FileUploadController) Merge(){
	//返回给前端的结构体
	resultInfo := FileUploadResponseInfo{}

	//接收变量
	fileMd5:=c.GetString("identifier")
	filename:=c.GetString("filename")
	username:=c.GetString("username")
	targetFileName:=fileMd5+filepath.Ext(filename)
	//根据username+md5查询数据库
	detail,err:=models.NewFileUploadDetail().FindUploadDetailByFileName(username,targetFileName)
	if err!=nil && err.Error()=="<QuerySeter> no row found"{
		fmt.Printf("merge: fail to findUploadDetailByFileName username:[%v] targetFileName:[%v]",username,targetFileName)
		return
	}

	//校验detail的isUploaded是否为1
	if detail.IsUploaded!=1{//说明还没上传完，没办法合并
		str:=fmt.Sprintf("fail to merge chunks because filename:[%v] isUploaded:[%v]",filename,detail.IsUploaded)
		resultInfo.Msg=str
		resultInfo.Status=1  //0成功，1失败
		c.Data["json"]=resultInfo
		c.ServeJSON()
		return
	}
	//校验detail的totalChunk是否和hasBeenUploaded去掉:后的顺序一致
	//arr:=strings.Split(detail.HasBeenUploaded,":")
	//if detail.TotalChunks!=len(arr){
	//	str := fmt.Sprintf("fail to merge chunks because some fragments that have not been uploaded filename:[%v] totalChunks:[%v] hasBeenUploaded:[%v]", targetFileName, detail.TotalChunks, detail.HasBeenUploaded)
	//	resultInfo.Msg = str
	//	resultInfo.Status = 1
	//	c.Data["json"] = resultInfo
	//	c.ServeJSON()
	//	return
	//}
	//已经传完并且数量一致，开始合并
	path:="/opt/data"
	baseUrl := path + "/upload/" + username+ "/"+ fileMd5 + "/"
	targetFileName=baseUrl+fileMd5+filepath.Ext(filename)  //完整路径

	// todo 考虑这里要不要查看下数据库中isUploaded的信息
	//如果没有上传完就请求merge那么就返回错误
	if detail.IsUploaded!=1{
		resultInfo.Merge=0
		resultInfo.Msg="can not found the file,please reupload"
		resultInfo.IsUploaded=0
		detail.DeleteOneRecord()
		c.Data["json"]=resultInfo
		c.ServeJSON()
		return
	}
	//创建临时文件
	f,err:=os.OpenFile(targetFileName,os.O_CREATE|os.O_APPEND|os.O_RDWR,0777)
	if err!=nil{
		fmt.Printf("error:%v",err)
		return
	}
	var totalSize int64  //用于记录文件的末尾，尾部插入合并文件（byte）
	writer:=bufio.NewWriter(f)
	//合并文件 /opt/data/upload/username/md5_chunkNum.EXT
	//先读取出所有符合md5命名格式的文件
	for i:=1;i<=detail.TotalChunks;i++{
		currentChunkFile:=baseUrl+fileMd5+"_"+strconv.Itoa(i)+filepath.Ext(filename)
		bytes,err:=ioutil.ReadFile(currentChunkFile)
		if err!=nil{
			fmt.Printf("error:%v",err)
			return
		}
		num,err:=writer.Write(bytes)
		if err!=nil{
			fmt.Printf("error:%v",err)
			return
		}
		totalSize+=int64(num)
		fmt.Printf("本次读取了 %v bytes",num)
		// todo 暂时选择移除, 等所有的工作都做完了，可以考虑这样：第一次merge失败了我们也不会删除，提示用户重新上传就好了，并在数据库中记录曾经merge失败过
		// todo 如果第二次merge又失败了，删除暂存的分片，删除用户的上传记录
		//读取之后删除当前切片
		err = os.Remove(currentChunkFile)
		if err!=nil{
			fmt.Printf("error:%v",err)
			return
		}
	}
	err = writer.Flush()
	if err!=nil{
		fmt.Printf("error:%v",err)
		return
	}
	//先关闭再打开验证md5
	f.Close()

	//重新打开文件，目的是获取合成的这个文件的md5
	f,err=os.OpenFile(targetFileName,os.O_CREATE|os.O_APPEND|os.O_RDWR,0777)
	if err!=nil{
		fmt.Printf("error:%v",err)
		return
	}
	defer f.Close()

	//开始计算md5
	md5:=md5.New()
	//对f文件进行md5加密
	writtenNum,err:=io.Copy(md5,f)  //复制指针，大文件，防止内存溢出，按照缓冲区32KB循环复制，不会将内容一次性全写入内存中
	if err!=nil{
		fmt.Printf("没办法复制到md5读中,error:%v",err)
		return
	}
	fmt.Printf("一共复制到md5中的bytes为%v",writtenNum)
	//将[]byte转为string
	md5Val:=hex.EncodeToString(md5.Sum(nil)) //nil返回的是[]byte，长度未定，先new在sum   //如果直接md5.sum，返回的是[size]byte

	if md5Val!=detail.Md5{
		// todo 删除文件及数据库中的记录
		err=os.Remove(targetFileName)
		if err!=nil{
			fmt.Printf("删除合并文件失败,error:%v",err)
		}
		id,err:=detail.DeleteOneRecord()
		if err!=nil || id<0{
			fmt.Printf("fail to delete upload detail record fineName:[%v] md5:[%v] err:[%v]", filename, fileMd5, err)
		}
		// md5不同
		resultInfo.Msg = "fileMd5Err"
		resultInfo.Status = 1
		c.Data["json"] = resultInfo
		c.ServeJSON()
		return
	}
	fmt.Println("file md5 is lawful ，will return fileUrl to fontEnd")
	// todo 将url更新到数据库
	detail.Url=targetFileName //完整的文件路径
	num,err:=detail.UpdateColumn("url")
	if err!=nil || num<0{
		fmt.Printf("fail to updateColumn err:[%v]",err)
		return
	}
	// 合并成功，给前端响应
	resultInfo.Msg = "Success merge the file"
	resultInfo.URL = targetFileName
	c.Data["json"] = resultInfo
	c.ServeJSON()
	return
}


























