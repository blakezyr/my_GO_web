package MRI

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"myweb/models"
	"strconv"
)

type PFISTAPngKy struct {
	beego.Controller
}

func (c *PFISTAPngKy) HanderPngKy(){
	//tokenData:=c.Ctx.Input.Header("Authorization")
	//result,_:=middleware.HandleToken(tokenData)
	//if result==false{ //没有token,无法提供服务
	//	c.Data["json"]= map[string]interface{}{
	//		"status_code":404,
	//	}
	//	c.ServeJSON()
	//}
	id,err:=c.GetInt("id")
	dia,_:=c.GetInt("dia")
	fmt.Println("dia is :",dia)
	tempId:=strconv.Itoa(id)+strconv.Itoa(dia)
	idInt,_:=strconv.Atoi(tempId)
	fmt.Println("idInt is:",idInt)
	if err!=nil{
		logs.Info("获取ID错误")
		return
	}
	o:=orm.NewOrm()
	arti:=models.ScoreKy{Id:id}
	err=o.Read(&arti)
	ReconUWT := arti.ReconUWTAddressKy
	ReconDWT :=arti.ReconDWTAddressKy
	Real := arti.RealAddressKy
	page := arti.Page
	evl:=models.Evl{Id: int64(idInt)}
	err=o.Read(&evl)
	index1:=evl.Index1
	index2:=evl.Index2
	index3:=evl.Index3
	index4:=evl.Index4
	index5:=evl.Index5
	index6:=evl.Index6
	fmt.Println("fing_png",index1)
	size:=models.Size{Id:id}
	err=o.Read(&size)
	row:=size.Col
	col:=size.Slice
	var ReconImgUWT []string
	var ReconImgDWT []string
	var RealImg []string

	//ReconImg := make(map[int]interface{})
	//RealImg := make(map[int]interface{})
	for i:=1;i<=page;i++{
		temp1:="/download"+Real+"RealKy"+strconv.Itoa(i)+".png"
		temp2:="/download"+ReconUWT+"ReconUWTKy"+strconv.Itoa(i)+".png"
		temp3:="/download"+ReconDWT+"ReconDWTKy"+strconv.Itoa(i)+".png"
		ReconImgUWT=append(ReconImgUWT,temp2)
		ReconImgDWT=append(ReconImgDWT,temp3)
		RealImg=append(RealImg,temp1)

	}
	index1Res:=SplitAs(index1)
	fmt.Println("fing_png_int",index1Res)
	index2Res:=SplitAs(index2)
	index3Res:=SplitAs(index3)
	index4Res:=SplitAs(index4)
	index5Res:=SplitAs(index5)
	index6Res:=SplitAs(index6)
	c.Data["json"]= map[string]interface{}{
		"recon_UWT":ReconImgUWT,
		"recon_DWT":ReconImgDWT,
		"real_map":RealImg,
		"pageLen":page,
		"index1":index1Res,
		"index2":index2Res,
		"index3":index3Res,
		"index4":index4Res,
		"index5":index5Res,
		"index6":index6Res,
		"row":row,
		"col":col,
		"dia":2,
	}
	c.ServeJSON()
}

