package MRI

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
	"log"
	"myweb/controllers/utils"
)
type GrpcConnection struct {
	conn    *grpc.ClientConn
	signal  chan int  //统计连接的数量
}

var (
	AddressGroup = []string{"0.0.0.0:10086"}
	ThreadNumber = len(AddressGroup)
	maxConnect=10
	//匿名函数，ConnectPool负责接收返回值
	ConnectPoll = func() []*GrpcConnection{
		connectPoll:=make([]*GrpcConnection,0,ThreadNumber)
		log:=utils.CreateLog("grpcGroup.log")
		log.Informational("start init grpc group!!!")

		for _,address:=range AddressGroup{
			conn,err:=grpc.Dial(address,grpc.WithInsecure())
			if err!=nil{
				panic(err)
			}
			grpcCreate:=&GrpcConnection{conn: conn}
			// 长度为2的通道, 预先读入2个值, 表示最大可用临界资源的数量为2
			grpcCreate.signal=make(chan int,maxConnect)
			//初始化写入Max个，说明目前Max个可用，如果没有内容说明已经用完
			for i:=0;i<maxConnect;i++{
				grpcCreate.signal <- 0
			}
			connectPoll=append(connectPoll,grpcCreate)
		}
		return connectPoll
	}()
)
//GrpcConnection是一个结构体，所以返回指针
func GetGrpcConnection() *GrpcConnection{
	for ;;{
		for num,connection:=range ConnectPoll{
			select {
			case <- connection.signal:
				newConn,err:=CheckConn(connection.conn,num)
				if err!=nil{
					log.Printf("connect error:%v\n",err)
					continue
				}
				connection.conn=newConn
				return connection
			default:
				continue
			}
		}
	}
}
//检查当前连接是否失效，失效就重新连接
func CheckConn(connect *grpc.ClientConn,num int)(*grpc.ClientConn,error){
	if connect.GetState()==connectivity.Shutdown{
		newConnection,err:=grpc.Dial(AddressGroup[num],grpc.WithInsecure())
		if err!=nil{
			return nil,err
		}
		return newConnection,nil
	}
	return connect,nil
}
