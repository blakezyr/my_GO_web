package MRI

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"myweb/models"
	"os"
	"path"
)

type PFISTADel struct {
	beego.Controller
}

func (c *PFISTADel) HanderDel (){
	//tokenData:=c.Ctx.Input.Header("Authorization")
	//result,_:=middleware.HandleToken(tokenData)
	//if result==false{ //没有token,无法提供服务
	//	c.Data["json"]= map[string]interface{}{
	//		"status_code":404,
	//	}
	//	c.ServeJSON()
	//}
	rootPath:=beego.AppConfig.String("rootPath")
	id,err:=c.GetInt("id")
	if err!=nil{
		logs.Info("获取ID错误")
		return
	}
	o:=orm.NewOrm()
	arti:=models.PfistaMulti{Id:id}
	evl:=models.Evl{Id: int64(id)}
	err=o.Read(&arti)
	currentPath1:=arti.Data
	currentPath2:=arti.Mask
	//currentPath1 ="/static/"+currentPath1
	//currentPath2 ="/static/"+currentPath2
	filePath1:=path.Join(rootPath,currentPath1)
	filePath2:=path.Join(rootPath,currentPath2)
	fmt.Println("filePath:",filePath1)
	err1:=os.Remove(filePath1)
	err2:=os.Remove(filePath2)
	if err1!=nil||err2!=nil{
		fmt.Println("删除文件错误")
		c.Data["json"]= map[string]interface{}{
			"status_code":"error",
		}
		c.ServeJSON()
	}else{
		fmt.Println("删除文件成功")
	}
	if err!=nil{
		logs.Info("没有查询到数据!")
		c.Data["json"]= map[string]interface{}{
			"status_code":"error",
		}
		c.ServeJSON()
	}
	o.Delete(&arti)
	o.Delete(&evl)
	c.Data["json"]= map[string]interface{}{
		"status_code":"success",
	}
	c.ServeJSON()
}