package MRI

import (
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"myweb/controllers/utils"
	"myweb/models"
)
type Register struct {
	beego.Controller

}
type UserInfo struct {
	Username string `json"username"`
	Email string `json"email"`
	Password string `json:"password"`
	Vcode string `json:"vcode"`
}
func (c *Register) Register(){
	userInfo:=&UserInfo{}
	json.Unmarshal(c.Ctx.Input.RequestBody,&userInfo)

	vcodeSeesion :=c.GetSession("vcode").(string)
	fmt.Println("vcodesession",vcodeSeesion)

	if !(userInfo.Vcode==vcodeSeesion){//验证码错误
		c.Data["json"]= map[string]interface{}{
			"status_code":-1,
		}
		c.ServeJSON()
	}
	o := orm.NewOrm()
	user := models.User{}
	fmt.Println("username",userInfo.Username)
	fmt.Println("password",userInfo.Password)
	fmt.Println("email",userInfo.Email)
	fmt.Println("vcode",userInfo.Vcode)
	user.Name=userInfo.Username
	err:=o.Read(&user,"Name")
	log:=utils.CreateLog("Register")
	log.Info("查到用户的信息为:",user)
	if err!=orm.ErrNoRows{ //用户名已存在
		c.Data["json"]= map[string]interface{}{
			"status_code":-2,
		}
		c.ServeJSON()
	}
	//用户名不存在
	user.Pwd=userInfo.Password
	user.Email=userInfo.Email
	_,err=o.Insert(&user)
	if err!=nil{
		fmt.Println("插入数据库失败")
		return
	}
	c.Data["json"]= map[string]interface{}{
		"status_code":0,
	}
	c.ServeJSON()
}