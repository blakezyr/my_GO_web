package MRI

import (
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"myweb/middleware"
	"myweb/models"
	"strings"
)
type LoginController struct {
	beego.Controller
}


type LoginInfo struct {
	Username string `json:"username"`
	Password string `json:"password"`
}


func (c *LoginController) HandleLogin() {

	tokenStr:=middleware.HandleJwt()
	logs.Info("tokenStr",tokenStr)
	// 1、获取数据
	logs.Info("当前来到登录界面后台处理！")
	loginInfo:=&LoginInfo{}
	json.Unmarshal(c.Ctx.Input.RequestBody,&loginInfo)
	userName := strings.Trim(loginInfo.Username,"") //去掉空格
	pwd := strings.Trim(loginInfo.Password,"")


	// 2、查询mysql数据库
	o := orm.NewOrm()
	user := models.User{}
	user.Name = userName
	err := o.Read(&user, "Name")
	logs.Info("查询到当前用户信息为：", user)
	if err == orm.ErrNoRows {
		logs.Info("用户名不存在!")
		c.Data["json"]= map[string]interface{}{
			"status_code":"failure",
		}
		c.ServeJSON()
	}
	if user.Pwd != pwd {
		logs.Info("密码错误!")
		c.Data["json"]= map[string]interface{}{
			"status_code":"failure",
		}
		c.ServeJSON()
	}
	c.Data["json"]= map[string]interface{}{
		"status_code":"success",
		"tokenStr":tokenStr,
	}
	c.ServeJSON()
}


