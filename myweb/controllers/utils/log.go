package utils

import (
	"encoding/json"
	"github.com/astaxie/beego/logs"
)
type LogT struct {
	Filename 	string 	`json:"filename"`
	Level 		int64 	`json:"level"`
	Maxlines 	int64 	`json:"maxlines"`
	Maxsize 	int64 	`json:"maxsize"`
}

func CreateLog(savePath string) *logs.BeeLogger{
	log:=logs.NewLogger(100)
	cfgLog:=&LogT{}
	cfgLog.Filename=savePath
	cfgLog.Level=logs.LevelDebug
	cfgLog.Maxlines=1000
	cfgLog.Maxsize=10240
	cfgJson,_:=json.Marshal(cfgLog)
	logs.Info(cfgJson)
	log.SetLogger("file",string(cfgJson))//设置记录日志的方式，本地文件记录
	log.EnableFuncCallDepth(true)     // 输出log时能显示输出文件名和行号（非必须）
	return log
}