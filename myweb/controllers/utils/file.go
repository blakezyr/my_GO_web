package utils

import (
	"io"
	"os"
)

func SaveToFile(reader io.Reader,byteSize int64,filename string) error{
	if byteSize==0{
		return nil
	}
	file,err:=os.Create(filename)
	if err!=nil{
		return err
	}
	log:=CreateLog("SaveFile.log")
	log.Informational("byteSize is:",byteSize)
	defer file.Close()
	bytes:=make([]byte,byteSize)
	//检查是否能读这么大的文件，能读才可以写
	readSize,err:=reader.Read(bytes)
	if err!=nil{
		log.Error("read file error:",err)
		return err
	}
	log.Informational("read size is ",readSize)
	if _,err=file.Write(bytes);err!=nil{
		return err
	}
	return nil
}
