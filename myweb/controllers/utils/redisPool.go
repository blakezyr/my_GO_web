package utils

import (
	"github.com/gomodule/redigo/redis"
)

// 定义一个全局的pool
var Pool *redis.Pool

func init() {
	Pool = &redis.Pool{
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", "127.0.0.1:6379")
			//return redis.Dial("tcp", "27.151.9.227:6379")
		},
		MaxIdle:     8,
		MaxActive:   0,   // 0表示没有限制
		IdleTimeout: 100, // 最大空闲时间
	}
}

