# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: pd.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='pd.proto',
  package='pb',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x08pd.proto\x12\x02pb\"E\n\x07Request\x12\x12\n\nfile1_path\x18\x01 \x01(\t\x12\x12\n\nfile2_path\x18\x02 \x01(\t\x12\x12\n\nconstant_R\x18\x03 \x01(\x05\"/\n\x05Reply\x12\x13\n\x0bresult_path\x18\x01 \x01(\t\x12\x11\n\tcost_time\x18\x02 \x01(\x02\"[\n\tNusReques\x12\x13\n\x0bnus_desnity\x18\x01 \x01(\x05\x12\x13\n\x0brandom_seed\x18\x02 \x01(\x05\x12\x10\n\x08\x66t1_file\x18\x03 \x01(\t\x12\x12\n\nmodel_type\x18\x04 \x01(\x05\"6\n\x08NusReply\x12\x14\n\x0cnuslist_path\x18\x01 \x01(\t\x12\x14\n\x0c\x66iddata_path\x18\x02 \x01(\t\"\xae\x02\n\tPreReques\x12\x0b\n\x03\x44sp\x18\x01 \x01(\x05\x12\x0e\n\x06\x44spOff\x18\x02 \x01(\t\x12\x0e\n\x06\x44spEnd\x18\x03 \x01(\t\x12\x0e\n\x06\x44spPow\x18\x04 \x01(\t\x12\x0c\n\x04\x44spC\x18\x05 \x01(\t\x12\x0b\n\x03\x44ps\x18\x06 \x01(\x05\x12\r\n\x05\x44psP0\x18\x07 \x01(\t\x12\r\n\x05\x44psP1\x18\x08 \x01(\t\x12\x0b\n\x03Idi\x18\t \x01(\x05\x12\x0b\n\x03Isp\x18\n \x01(\x05\x12\x0e\n\x06IspOff\x18\x0b \x01(\t\x12\x0e\n\x06IspEnd\x18\x0c \x01(\t\x12\x0e\n\x06IspPow\x18\r \x01(\t\x12\x0c\n\x04IspC\x18\x0e \x01(\t\x12\x0b\n\x03Ips\x18\x0f \x01(\x05\x12\r\n\x05IpsP0\x18\x10 \x01(\t\x12\r\n\x05IpsP1\x18\x11 \x01(\t\x12\x10\n\x08\x44\x61taType\x18\x12 \x01(\x05\x12\x16\n\x0eRawDataAddress\x18\x13 \x01(\t\".\n\x08PreReply\x12\x10\n\x08\x66t1_path\x18\x01 \x01(\t\x12\x10\n\x08img_path\x18\x02 \x01(\t2\x8c\x01\n\x07Greeter\x12*\n\x0eReconstruction\x12\x0b.pb.Request\x1a\t.pb.Reply\"\x00\x12(\n\x07NusData\x12\r.pb.NusReques\x1a\x0c.pb.NusReply\"\x00\x12+\n\nPreProcess\x12\r.pb.PreReques\x1a\x0c.pb.PreReply\"\x00\x62\x06proto3'
)




_REQUEST = _descriptor.Descriptor(
  name='Request',
  full_name='pb.Request',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='file1_path', full_name='pb.Request.file1_path', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='file2_path', full_name='pb.Request.file2_path', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='constant_R', full_name='pb.Request.constant_R', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=16,
  serialized_end=85,
)


_REPLY = _descriptor.Descriptor(
  name='Reply',
  full_name='pb.Reply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='result_path', full_name='pb.Reply.result_path', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='cost_time', full_name='pb.Reply.cost_time', index=1,
      number=2, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=87,
  serialized_end=134,
)


_NUSREQUES = _descriptor.Descriptor(
  name='NusReques',
  full_name='pb.NusReques',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='nus_desnity', full_name='pb.NusReques.nus_desnity', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='random_seed', full_name='pb.NusReques.random_seed', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='ft1_file', full_name='pb.NusReques.ft1_file', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='model_type', full_name='pb.NusReques.model_type', index=3,
      number=4, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=136,
  serialized_end=227,
)


_NUSREPLY = _descriptor.Descriptor(
  name='NusReply',
  full_name='pb.NusReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='nuslist_path', full_name='pb.NusReply.nuslist_path', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='fiddata_path', full_name='pb.NusReply.fiddata_path', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=229,
  serialized_end=283,
)


_PREREQUES = _descriptor.Descriptor(
  name='PreReques',
  full_name='pb.PreReques',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='Dsp', full_name='pb.PreReques.Dsp', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DspOff', full_name='pb.PreReques.DspOff', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DspEnd', full_name='pb.PreReques.DspEnd', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DspPow', full_name='pb.PreReques.DspPow', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DspC', full_name='pb.PreReques.DspC', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Dps', full_name='pb.PreReques.Dps', index=5,
      number=6, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DpsP0', full_name='pb.PreReques.DpsP0', index=6,
      number=7, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DpsP1', full_name='pb.PreReques.DpsP1', index=7,
      number=8, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Idi', full_name='pb.PreReques.Idi', index=8,
      number=9, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Isp', full_name='pb.PreReques.Isp', index=9,
      number=10, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='IspOff', full_name='pb.PreReques.IspOff', index=10,
      number=11, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='IspEnd', full_name='pb.PreReques.IspEnd', index=11,
      number=12, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='IspPow', full_name='pb.PreReques.IspPow', index=12,
      number=13, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='IspC', full_name='pb.PreReques.IspC', index=13,
      number=14, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='Ips', full_name='pb.PreReques.Ips', index=14,
      number=15, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='IpsP0', full_name='pb.PreReques.IpsP0', index=15,
      number=16, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='IpsP1', full_name='pb.PreReques.IpsP1', index=16,
      number=17, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='DataType', full_name='pb.PreReques.DataType', index=17,
      number=18, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='RawDataAddress', full_name='pb.PreReques.RawDataAddress', index=18,
      number=19, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=286,
  serialized_end=588,
)


_PREREPLY = _descriptor.Descriptor(
  name='PreReply',
  full_name='pb.PreReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='ft1_path', full_name='pb.PreReply.ft1_path', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='img_path', full_name='pb.PreReply.img_path', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=590,
  serialized_end=636,
)

DESCRIPTOR.message_types_by_name['Request'] = _REQUEST
DESCRIPTOR.message_types_by_name['Reply'] = _REPLY
DESCRIPTOR.message_types_by_name['NusReques'] = _NUSREQUES
DESCRIPTOR.message_types_by_name['NusReply'] = _NUSREPLY
DESCRIPTOR.message_types_by_name['PreReques'] = _PREREQUES
DESCRIPTOR.message_types_by_name['PreReply'] = _PREREPLY
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Request = _reflection.GeneratedProtocolMessageType('Request', (_message.Message,), {
  'DESCRIPTOR' : _REQUEST,
  '__module__' : 'pd_pb2'
  # @@protoc_insertion_point(class_scope:pb.Request)
  })
_sym_db.RegisterMessage(Request)

Reply = _reflection.GeneratedProtocolMessageType('Reply', (_message.Message,), {
  'DESCRIPTOR' : _REPLY,
  '__module__' : 'pd_pb2'
  # @@protoc_insertion_point(class_scope:pb.Reply)
  })
_sym_db.RegisterMessage(Reply)

NusReques = _reflection.GeneratedProtocolMessageType('NusReques', (_message.Message,), {
  'DESCRIPTOR' : _NUSREQUES,
  '__module__' : 'pd_pb2'
  # @@protoc_insertion_point(class_scope:pb.NusReques)
  })
_sym_db.RegisterMessage(NusReques)

NusReply = _reflection.GeneratedProtocolMessageType('NusReply', (_message.Message,), {
  'DESCRIPTOR' : _NUSREPLY,
  '__module__' : 'pd_pb2'
  # @@protoc_insertion_point(class_scope:pb.NusReply)
  })
_sym_db.RegisterMessage(NusReply)

PreReques = _reflection.GeneratedProtocolMessageType('PreReques', (_message.Message,), {
  'DESCRIPTOR' : _PREREQUES,
  '__module__' : 'pd_pb2'
  # @@protoc_insertion_point(class_scope:pb.PreReques)
  })
_sym_db.RegisterMessage(PreReques)

PreReply = _reflection.GeneratedProtocolMessageType('PreReply', (_message.Message,), {
  'DESCRIPTOR' : _PREREPLY,
  '__module__' : 'pd_pb2'
  # @@protoc_insertion_point(class_scope:pb.PreReply)
  })
_sym_db.RegisterMessage(PreReply)



_GREETER = _descriptor.ServiceDescriptor(
  name='Greeter',
  full_name='pb.Greeter',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=639,
  serialized_end=779,
  methods=[
  _descriptor.MethodDescriptor(
    name='Reconstruction',
    full_name='pb.Greeter.Reconstruction',
    index=0,
    containing_service=None,
    input_type=_REQUEST,
    output_type=_REPLY,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='NusData',
    full_name='pb.Greeter.NusData',
    index=1,
    containing_service=None,
    input_type=_NUSREQUES,
    output_type=_NUSREPLY,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
  _descriptor.MethodDescriptor(
    name='PreProcess',
    full_name='pb.Greeter.PreProcess',
    index=2,
    containing_service=None,
    input_type=_PREREQUES,
    output_type=_PREREPLY,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_GREETER)

DESCRIPTOR.services_by_name['Greeter'] = _GREETER

# @@protoc_insertion_point(module_scope)
