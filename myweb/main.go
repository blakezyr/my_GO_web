package main

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
	_ "myweb/controllers/utils" //执行init（）
	"myweb/models"
	_ "myweb/models"
	_ "myweb/routers"
)
func init(){
	//配置允许跨域
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins: []string{"*"},  //允许跨域的域，就是允许哪些域名能访问跨域的url
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},  //允许请求的类型
		AllowHeaders: []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		ExposeHeaders: []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		AllowCredentials: true,  //是否允许cookie跨域
	}))
}
func main() {
	beego.AddFuncMap("ShowPrePage",models.HandlePrePage)   //必须在run之前调用映射
	beego.AddFuncMap("ShowNextPage",models.HandleNextPage)
	beego.Run()
}

