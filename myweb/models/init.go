package models

import "github.com/astaxie/beego/orm"

func init(){
	//设置数据库基本信息(别名、驱动名称（mysql，memcache）、
	//本地
	//orm.RegisterDataBase("default","mysql","root:root@tcp(127.0.0.1:3306)/test?charset=utf8")
	orm.RegisterDataBase("default","mysql","root:CSG@MYSQL-SERVICE@tcp(127.0.0.1:3306)/test?charset=utf8")
	//orm.RegisterDataBase("default","mysql","root:123456@tcp(127.0.0.1:3306)/test?charset=utf8")
	//远程（需要自己去安全组添加3306端口，否则没办法访问）
	//orm.RegisterDataBase("default","mysql","root:123456@tcp(27.151.9.227:3306)/test?charset=utf8")
	//映射model数据(如果没有表，需要自己new一个）
	orm.RegisterModel(new(User),new(Download),new(Nus),new(Result),new(PFISTA),new(PfistaMulti),new(PfistaMultiRes),new(Score),new(Evl),new(Size),new(ScoreKx),new(ScoreKy),new(FileUploadDetail))
	//生成表(别名，是否强制更新，是否可见）
	_=orm.RunSyncdb("default",false,true)
}
