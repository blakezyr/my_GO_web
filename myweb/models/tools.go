package models

import (
	"crypto/md5"
	"encoding/hex"
	"time"
)

//存放公共映射
func HandlePrePage(data int)(int){
	pageIndex:=data-1
	return pageIndex
}
func HandleNextPage(data int)(int){
	pageIndex:=data+1
	return pageIndex
}
func GetUnix() int64{   //放公共方法，负责返回时间撮
	return time.Now().Unix()
}
func UnixToDate(timestamp int) string {

	t := time.Unix(int64(timestamp), 0)

	return t.Format("2006-01-02 15:04:05")
}
func GetData() string{
	template:= "2006-01-02 15:04:05"
	return time.Now().Format(template)
}


//封装md5加密
/*func Md5(str string) string {
	data:= []byte(str)
	return fmt.Sprint("%x\n",md5.Sum(data))
}*/
func Md5(str string) string {
	m := md5.New()
	m.Write([]byte(str))
	return string(hex.EncodeToString(m.Sum(nil)))
}