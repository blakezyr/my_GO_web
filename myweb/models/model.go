package models

import (
	"fmt"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

//表的设计
type User struct {
	Id int
	Email string
	Name string `orm:"unique"`
	Pwd string
}

type Download struct{
	Id int `orm:"pk"`   //主键+自增 ;auto
	Filename string
	Address string
	CostTime float64
	TxtFile string
	Filename2 string
}
type Nus struct {
	Id int `orm:"pk"`
	Filename string `orm:"size(20)"`
	Random  int
	Address1 string
	Address2 string
}
type Result struct{
	Id int `orm:"pk"`
	Filename string `orm:"size(20)"`
	Ft1Path string
	ImgPath string
}
type PFISTA struct {
	Id int `orm:"pk"`
	Filename string
	Lable string
	Recon string
	Address string
}
type PfistaMulti struct {
	Id int `orm:"pk"`
	Filename string `orm:"size(20)"`
	Username string `orm:"unique"`
	ResAddress string
	RealAddress string
	Data string
	Mask string
	ResUWTPng string
	ResDWTPng string
	Score  int
	Page int
}
type Score struct {
	Id int `orm:"pk"`
	RealAddress string
	ReconUWTAddress string
	ReconDWTAddress string
	Page int
}
type ScoreKy struct {
	Id int `orm:"pk"`
	RealAddressKy string
	ReconUWTAddressKy string
	ReconDWTAddressKy string
	Page int
}
type ScoreKx struct {
	Id int `orm:"pk"`
	RealAddressKx string
	ReconUWTAddressKx string
	ReconDWTAddressKx string
	Page int
}
type PfistaMultiRes struct {
	Id int `orm:"pk"`
	ResAdd string
	PngAdd string
}
type Evl struct {
	Id int64 `orm:"pk"`
	Index1 string	`orm:"size(600)"`
	Index2 string	`orm:"size(600)"`
	Index3 string	`orm:"size(600)"`
	Index4 string	`orm:"size(600)"`
	Index5 string	`orm:"size(600)"`
	Index6 string	`orm:"size(600)"`
}
type Size struct {
	Id int `orm:"pk"`
	Slice int
	Row int
	Col int
}
type FileUploadDetail struct {
	Id              int       `json:"id" orm:"column(id);pk;auto"`
	Username        string    `json:"username" orm:"column(username);size(64)"`
	FileName        string    `json:"fileName" orm:"column(file_name);size(64)"`
	Md5             string    `json:"md5" orm:"column(md5);size(255)"`
	IsUploaded      int       `json:"isUploaded" orm:"column(is_uploaded)"`
	TotalChunks     int       `json:"totalChunks" orm:"column(total_chunks)"`
	//HasBeenUploaded string    `json:"hasbeenUploaded" orm:"column(has_been_uploaded);size(10240)"`
	Url             string    `json:"url" orm:"column(url);size(255)"`
	CreateTime      time.Time `json:"createTime" orm:"column(create_time);type(datetime);auto_now_add"`
	UpdateTime      time.Time `json:"updateTime" orm:"column(update_time);type(datetime);"`
	//ChunkNumbers  	int 	  `json:"ChunkNumbers" orm:"column(chunk_numbers)"`
}
func (f *FileUploadDetail) TableName() string{
	return "file_upload_detail"
}


//根据username 和 filename 查询文件细节
func (f *FileUploadDetail) FindUploadDetailByFileName(username,filename string) (detail FileUploadDetail,err error) {
	newOrm:= orm.NewOrm()
	err=newOrm.QueryTable(&FileUploadDetail{}).Filter("username",username).Filter("filename",filename).One(&detail)
	return
}


//保存一条记录
func (f *FileUploadDetail) InsertOneRecord() (id int64,err error){
	id,err=orm.NewOrm().Insert(f)
	return
}


//md5验证失败，需要删除这条全部记录
func (f *FileUploadDetail) DeleteOneRecord() (id int64,err error){
	id,err=orm.NewOrm().Delete(f)
	return
}

//更新一列
func (f *FileUploadDetail) UpdateColumn(uploaded string) (num int64,err error){  //更换forupdate
	newOrm:=orm.NewOrm()
	num,err=newOrm.Update(f,uploaded)

	if err!=nil{
		fmt.Printf("fail to update fileUploadDetail one column,columnName:[%v]",uploaded)
		return
	}
	return
}
//更新一列(带锁)
func (f *FileUploadDetail) UpdateColumnForUpdate(uploaded string) (err error){  //更换forupdate
	newOrm:=orm.NewOrm()
	// todo 已经上传的文件数量+1
	newOrm.Raw("")
	newOrm.Begin()
	newOrm.Raw("select *from file_upload_detail where id=?",f.Id) //主键，行级锁
	newOrm.Commit()
	err=newOrm.ReadForUpdate(f,uploaded)
	if err!=nil{
		fmt.Printf("fail to update fileUploadDetail one column,columnName:[%v]",uploaded)
		return
	}
	return
}

func NewFileUploadDetail() *FileUploadDetail {
	return &FileUploadDetail{}
}






