package middleware

import (
	"dgrijalva/jwt-go"
	"errors"
	"fmt"
	"github.com/astaxie/beego/context"
	"github.com/astaxie/beego/logs"
	"strings"
	"time"
) //导入的是beego下的contex包不是自带的contex包
type MyClaim struct {
	jwt.StandardClaims
}



const (
	SecretKEY              string = "Blake Griffin Is From Oklahoma"
	DEFAULT_EXPIRE_SECONDS int    = 24 // default expired 1 minute
)


func AdminAuth(ctx *context.Context){
	pathName:=ctx.Request.URL.String()
	userName,ok:=ctx.Input.Session("userName").(string)
	if !(ok&&userName!=""){
		if pathName!="/login"{
			ctx.Redirect(302,"/login")
		}
	}
}
//Generate
func HandleJwt()string{
	//1、实例化自定义结构体
	expireAt := time.Now().Add(time.Hour * time.Duration(DEFAULT_EXPIRE_SECONDS)).Unix()
	myClaimsObj :=&MyClaim{
		jwt.StandardClaims{
			ExpiresAt: expireAt,
			Issuer: "blake",
			IssuedAt: time.Now().Unix(),
		},
	}
	//2、使用指定的签名方法创建签名对象
	tokenObj:=jwt.NewWithClaims(jwt.SigningMethodHS256,myClaimsObj)

	//3、使用指定的secret签名并获得完整的编码后的字符串token
	tokenStr,err:=tokenObj.SignedString([]byte(SecretKEY))
	if err!=nil{
		fmt.Println("can not signedString,err:",err)
	}
	return tokenStr
}
//update token
func RefreshToken() (newTokenString string, err error) {


	mySigningKey := []byte(SecretKEY)
	expireAt := time.Now().Add(time.Hour * time.Duration(DEFAULT_EXPIRE_SECONDS)).Unix() //new expired
	newClaims := MyClaim{

		jwt.StandardClaims{
			Issuer:    "blake", //name of token issue
			IssuedAt:  time.Now().Unix(),            //time of token issue
			ExpiresAt: expireAt,// new expired
		},
	}

	// generate new token with new claims
	newToken := jwt.NewWithClaims(jwt.SigningMethodHS256, newClaims)
	// sign the token with a secret
	tokenStr, err := newToken.SignedString(mySigningKey)
	if err != nil {
		fmt.Println("2222222222222222222")
		return "", errors.New("error: failed to generate new fresh json web token")
	}
	fmt.Println("3333333333333333333333333333")
	fmt.Println("tokenStr",tokenStr)
	return tokenStr, nil
}
//验证token
func ParseToken(tokenString string)(*jwt.Token,*MyClaim,error){
	s:=&MyClaim{}
	token,err:=jwt.ParseWithClaims(tokenString,s, func(token *jwt.Token) (i interface{}, err error) {
		return []byte(SecretKEY),nil
	})
	//fmt.Println("token is:",token)
	return token,s,err
}
func HandleToken(tokenData string)(bool,error){

	//fmt.Println("tokenData",tokenData)
	tokenString:=strings.Split(tokenData," ")[1] //Bearer Token
	//fmt.Println("tokenStr",tokenString)
	if tokenString==""{
		fmt.Println("权限不足")
		return false,nil
	}else{
		token,_,err:=ParseToken(tokenString)
		if err!=nil||!token.Valid{
			fmt.Println("权限不足")
			return false,err
		}else{
			//fmt.Println("验证通过,token正确")
			return true,nil
		}
	}
}

func FilterToken(ctx *context.Context){

	logs.Info("current router path is ", ctx.Request.RequestURI)
	if ctx.Request.RequestURI != "/login" && ctx.Input.Header("Authorization") == "" {
		logs.Error("without token, unauthorized !!")
		ctx.ResponseWriter.WriteHeader(401)
		ctx.ResponseWriter.Write([]byte("no permission"))
	}
	if ctx.Request.RequestURI != "/login" && ctx.Input.Header("Authorization") != "" {
		tokenData := ctx.Input.Header("Authorization")

		logs.Info("curernttoken: ", tokenData)
		result,err:=HandleToken(tokenData)
		if result==false{ //没有token,无法提供服务
			logs.Info("错误,err:",err)

			newToken, err2 := RefreshToken()
			if err2!=nil{
				fmt.Println("token can not RefreshToken,err is:",err2)
			}
			fmt.Println("newToken",newToken)
			ctx.ResponseWriter.WriteHeader(200)
			fmt.Println("newToken is :",newToken)
			ctx.ResponseWriter.Write([]byte("401"))
		}
		//成功认证，进入后端
		// validate token
		// invoke ValidateToken in utils/token
		// invalid or expired todo res 401
	}

}
